#!/bin/bash

function build_deps() {
	# echo "Building perf ---------------------------------------------------------"
	# sudo apt install linux-tools-common linux-tools-generic linux-tools-$(uname -r) -y
	# sudo sh -c 'echo -1 >/proc/sys/kernel/perf_event_paranoid'

	echo "Building collectl -----------------------------------------------------"
	tar -xzf collectl-4.3.1.src.tar.gz
	cd collectl-4.3.1
	sudo sh ./INSTALL
	cd ..

	echo "Building pssh -----------------------------------------------------"
	sudo apt install pssh -y

	if command -v jtm &>/dev/null; then
		echo jtm Already Exist!
	else
		echo "Building JTM ----------------------------------------------------------"
		git clone https://github.com/ldn-softdev/jtm.git
		cd jtm
		c++ -o jtm -Wall -std=gnu++14 -static -Ofast jtm.cpp
		sudo cp ./jtm /usr/local/bin/
		cd ..
	fi

}


build_deps

