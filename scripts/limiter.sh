#!/bin/bash
###################################################################
#Description	: Spark Profiling and Monitoring.
#Args           : HiBench parameters
#Authors       	:
#                 M.R. Siavash Katebzadeh (m.r.katebzadeh@ed.ac.uk)
###################################################################
bandwidths=("5" "10" "25" "50" "75" "90" "100")
limit=100

HOME_PATH=/users/${USER}
EX_PATH=${HOME_PATH}/k8s-bigdata
LOG_PATH=${EX_PATH}/logs
RES_PATH=${EX_PATH}/results

mkdir -p ${LOG_PATH}
mkdir -p ${RES_PATH}

LOG_FILE=${LOG_PATH}/experiment.log
ERR_FILE=${LOG_PATH}/experiment.err
PERF_LOG=${LOG_PATH}/perf.log
HOST_FILE=${LOG_PATH}/hosts


NOCOLOR='\033[0m'
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
YELLOW='\033[0;33m'
GRAY='\033[0;37m'
CYAN='\033[0;36m'

echo_color() {
    local color=$1
    local text=$2
    echo -e "${YELLOW}[$(date)]: ${color}${text}${NOCOLOR}"
    
    if [ "$3" = "NOERR" ]; then
        return
    fi
    echo "[$(date)]: ${text}" >>${LOG_FILE}
    if [ $color = ${RED} ]; then
        echo "[$(date)]: ${text}" >>${ERR_FILE}
    fi
}

function limit_bandwidth() {
    local perc=$1
    local interfaces=( "enp3s0f0" "enp3s0f1" )
    local maxspeed=$(cat /sys/class/net/enp3s0f0/speed)
    local limitspeed=$(echo "scale=2; (${perc} / 100) * ${maxspeed}" | bc)
    local finalbw=${limitspeed%.*}000
    
    [ ! -f ${HOST_FILE} ] && kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName --all-namespaces | awk '{ printf "%s\n", $3}' | sort | uniq | sed -e 1,1d > ${HOST_FILE}
    
    for interface in "${interfaces[@]}"
    do
        :
        bwcommand="'sudo wondershaper -a ${interface} -d ${finalbw} -u ${finalbw}'"
        
        echo_color ${GREEN} "Setting BW to ${finalbw} kbps"
        parallel-ssh -i -h ${HOST_FILE} ${bwcommand}
    done
}

function relax_bandwidth() {
    local interfaces=( "enp3s0f0" "enp3s0f1" )
    
    [ ! -f ${HOST_FILE} ] && kubectl get pod -o=custom-columns=NAME:.metadata.name,STATUS:.status.phase,NODE:.spec.nodeName --all-namespaces | awk '{ printf "%s\n", $3}' | sort | uniq | sed -e 1,1d > ${HOST_FILE}
    
    for interface in "${interfaces[@]}"
    do
        :
        flushcommand="'sudo ip addr flush dev ${interface}'"
        bwcommand="'sudo wondershaper -c -a ${interface}'"
        
        echo_color ${GREEN} "Flushing interface ${interface}"
        parallel-ssh -i -h ${HOST_FILE} ${flushcommand}
        
        echo_color ${GREEN} "Setting BW to 100%"
        parallel-ssh -i -h ${HOST_FILE} ${bwcommand}
    done
}


function test_dependencies() {
    if command -v parallel-ssh &>/dev/null; then
        echo_color ${GREEN} "Parallel-ssh checked!"
    else
        echo_color ${RED} "Please first install parallel-ssh. Hint: apt install pssh"
        exit 1
    fi
}

function check_parameters() {
    local err=0
    
    if [[ " ${bandwidths[*]} " =~ " ${limit} " ]]; then
        echo_color ${GREEN} "BW limit checked: ${limit}"
    else
        echo_color ${RED} "\"${limit}\" does not exists in the available BW values. Hint: ${bandwidths[*]}"
        err=1
    fi
    
    if [ $err = 1 ]; then
        exit 1
    fi
    
}

function showhelp() {
    echo "limiter"
    echo "NAME
       Limit link capacity
SYNOPSIS
      spark_monitor [options]
OPTIONS
    -l <value>, --limit <value>

        Available BW values are:
            - 100 (no limit)
            - 90
            - 75
            - 50
            - 25
            - 10
            - 5

    -h, --help

        Show this message.
    "
    command exit
}

function exit() {
    if [ $1 = "0" ]; then
        echo_color ${GREEN} "BW configuration finished"
    else
        echo_color ${RED} "Setting BW produced error." "NOERR"
    fi
    
    command exit
    
}

parse_args() {
    case "$1" in
        -l | --limit)
            limit="$2"
        ;;
        -h | --help)
            showhelp
        ;;
        *)
            echo_color ${RED} "Unknown parameter '$1'." 1>&2
            showhelp
            exit 1
        ;;
    esac
}

while [[ "$#" -ge 1 ]]; do
    parse_args "$1" "$2"
    shift
    shift
done

test_dependencies

check_parameters

relax_bandwidth

if [ $limit != 100 ]; then
    limit_bandwidth $limit
fi

exit 0

