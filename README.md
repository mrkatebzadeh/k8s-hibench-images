# k8s-bigdata
Apache Spark with HDFS cluster within Kubernetes.

### Overview
As the description says, this repository is an Apache Spark with an HDFS cluster within Kubernetes. Although it contains [Intel HiBench](https://github.com/Intel-bigdata/HiBench) benchmark suite for testing CPU, IO, and network usage, the cluster can run as a regular one. 

### Supported HiBench Workloads
- Micro
- Machine Learning
- Websearch

### Building k8s-bigdata
You can just execute the `build.sh` file.
```sh
$ ./build.sh
```

### Submitting the cluster
To submit the cluster and prepare it, you must type the following `./scripts/init-cluster.sh <WORKLOAD> <BENCHMARK> <INPUT_SIZE>`
Where:
1. `WORKLOAD` represents a workload from [HiBench](https://github.com/Intel-bigdata/HiBench)
2. `BENCHMARK` represents the benchmark 
3. `INPUT_SIZE` means the size of the workload for the benchmark

### Running HiBench
To run a [HiBench](https://github.com/Intel-bigdata/HiBench) benchmark, you can run `./scripts/run.sh <WORKLOAD> <BENCHMARK>`
The report saved will be in the base directory with the name `hibench.report`.

#### Example:
Here, we are submitting the cluster with `ml` WORKLOAD, `linear` BENCHMARK and `tiny` INPUT_SIZE or dataset size.

> Note: The dataset size must be one the following keywords: tiny, small, large, huge, gigantic and bigdata.


```sh
node-0:~/k8s-hibench-images>./scripts/init-cluster.sh ml linear tiny
```

### Features
- k8s-bigdata currently uses Apache Spark 2.4 with Hadoop 2.7 binary
- No need to register manually each `datanode`
- Kubernetes will create a `datanode` for each node registered in the cluster
- Can specify which node `namenode`, `resourcemanager`, and `historyserver` will be launched by assigning the label `type=master`. If you are new to Kubernetes, type `kubectl label nodes YOUR NODE type=master`

### References
- [Intel HiBench](https://github.com/Intel-bigdata/HiBench#hibench-suite-)
- [Docker Hadoop](https://github.com/big-data-europe/docker-hadoop)
- [Docker Spark](https://github.com/big-data-europe/docker-spark)
- [Apache Spark](https://spark.apache.org/)
- [Apache Hadoop](https://hadoop.apache.org/)
